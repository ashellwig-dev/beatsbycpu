# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import shutil
import os
import logging

import youtube_dl

_logger = logging.getLogger(__name__)


def dl_hook(d):
    if d['status'] == 'finished':
        _logger.info('Download complete. Converting...')


def download_playlist(download_url: str, download_dest: str):
    ydl_opts = {
        "extractaudio": True,
        "format": "bestaudio[ext=wav]/bestaudio[ext=mp3]/bestaudio[ext=m4a]",
        "noplaylist": False,
        "progress_hooks": [dl_hook],
        "prefer_ffmpeg": True,
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([download_url])
        for file in os.listdir(os.getcwd()):
            if file.endswith("webm") or file.endswith("wav") or file.endswith(
                    "mp4") or file.endswith("m4a"):
                destination = os.path.join(os.getcwd(), download_dest)

                if not os.path.exists(destination):
                    os.mkdir(destination)

                _logger.info(f'Moving {file} to {destination}')
                shutil.move(file, os.path.join(destination, file))
