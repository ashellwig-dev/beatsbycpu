#!/user/ahellwig/anaconda3/envs/bin/python
# -*- coding: utf-8 -*-
"""
This is the main run file for the package/application.

Run `python setup.py install` which will install the command `beatsbycpu`
inside your current environment.
Besides console scripts, the header (i.e. until _logger...) of this file can
also be used as template for Python modules.
"""

import argparse
import sys
import logging

from beatsbycpu import __version__
from beatsbycpu.downloader import download_playlist

_logger = logging.getLogger(__name__)


def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(description="Utilities to generate music")
    parser.add_argument("--version",
                        action="version",
                        version="beatsbycpu {ver}".format(ver=__version__))
    parser.add_argument("-v",
                        "--verbose",
                        dest="loglevel",
                        help="set loglevel to INFO",
                        action="store_const",
                        const=logging.INFO)
    parser.add_argument("-vv",
                        "--very-verbose",
                        dest="loglevel",
                        help="set loglevel to DEBUG",
                        action="store_const",
                        const=logging.DEBUG)
    parser.add_argument(
        "-d",
        "--download",
        action="store",
        dest="download_url",
        help="Download a youtube playlist and extract its audio.")
    parser.add_argument("-o",
                        "--output",
                        action="store",
                        dest="download_dest",
                        help="Folder to store downloaded media.")

    return parser


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(level=loglevel,
                        stream=sys.stdout,
                        format=logformat,
                        datefmt="%Y-%m-%d %H:%M:%S")


def main(args):
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list
    """
    parser = parse_args(args)
    args = parser.parse_args(args)
    setup_logging(args.loglevel)

    if args.download_url is not None:
        if args.download_dest is None:
            parser.error(
                "Please specify a download destination when using the download function."  # noqa: E501
            )
        else:
            download_playlist(args.download_url, args.download_dest)


def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
