==========
beatsbycpu
==========

Computer-generated electronic dance music.

Description
===========

This is the first attempt at creating a package which takes massive audio
input (generally .wav files) and generates similar music using a convolutional
neural network. It also contains tools and libraries for bulk-downloading
of youtube videos and extracting the audio only.

This is a work in progress.

Note
====

This project has been set up using PyScaffold 3.2.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
